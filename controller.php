<?php 
$errors = [];
$name=isset($_GET['name'])?$_GET['name']:"";
$lastname=isset($_GET['lastname'])?$_GET['lastname']:"";
$date=isset($_GET['date'])?$_GET['date']:"";
$email=isset($_GET['email'])?$_GET['email']:"";

if($name=="" || $lastname==''|| $date=="" || $email==""){
    $errors['form'] = '<b>Fill in all fields!<b><br>';
    include_once 'register.php';
}
if(!(is_numeric($date) && $date>1930 && $date<2004)){
    $errors['date']= '<b>Birth year is not valid!</b><br>';
    include_once 'register.php';
}

if (count($errors) == 0){
    include_once 'person.php';
}
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
<script>
$(function() {
$("form[name='registration']").validate({
rules: {
  name: "required",
  lastname: "required",
  date: "required",
  email: {
        required: true,
        email: true
      },

},
messages: {
    name: "Please enter your firstname",
      lastname: "Please enter your lastname",
      date: "Please enter birth year",
      email: "Please enter a valid email address"
    },
submitHandler: function(form) {
  form.submit();
}
});
});
</script>
</body>
<?php 
function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>