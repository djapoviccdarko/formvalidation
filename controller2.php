<?php
require_once 'car-class.php';
require_once 'car-controller.php';



$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : "";

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // get akcije
    switch ($action) {
        case 'View cars':
            $carController=new CarController();
            $carController->viewCarsButton();
            break;
        case 'Choose day':
            $carController=new CarController();
            $carController->chooseDayButton();
            break;
        case 'delete':
                $carController=new CarController();
                $carController->delete();
                break;
                //ovo
                case 'edit':
                    $carController=new CarController();
                    $carController->edit();
                    break;
                    case 'Prikaz zaduzenja':
                        $carController = new CarController();
                        $carController->prikazZaduzenja();
                        break;
                   
        default:
            echo 'ERROR WRONG GET ACTION';
            break;
    }
} elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // post akcije
    switch ($action) {
        case 'Send':
           $carController=new CarController();
           $carController->sendButton();
            break;
            case 'Edit':
    
                $carController = new CarController();
                $carController->saveEdit();
    
                break;
                case 'View all':
                    $carController=new CarController();
                    $carController->viewAllButton();
                    break;
        default:
            echo 'ERROR WRONG POST ACTION';
            break;
    }
} else {
    echo 'ERROR METHOD';
}
//Front Validation
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
<script>
$(function() {
$("form[name='addcar']").validate({
rules: {
  car: "required",
  price: "required",
  year: "required",
  date: "required",

},
messages: {
  car: "Please enter car brand",
  price: "Please enter car price",
  year: "Please enter a year",
  date: "Please enter a valid date",
},
submitHandler: function(form) {
  form.submit();
}
});
});
</script>
</body>
<?php 
function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>