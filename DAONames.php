<?php
require_once 'db.php';

class DAONames {
	private $db;

	// za 2. nacin resenja
	private $INSERT_NAME = "INSERT INTO names (name) VALUES (?)";
	private $SELECT_NAME = "SELECT * FROM names";
	private $SELECTBYNAME = "SELECT * FROM persons JOIN cars_persons ON persons.id_person=cars_persons.id_person JOIN cars ON cars_persons.id_car=cars.id_car JOIN names on cars.id_name = names.id_name WHERE names.id_name=?";

	
	public function __construct()
	{
		$this->db = DB::createInstance();
	}

	public function selectNames()
	{
		
		$statement = $this->db->prepare($this->SELECT_NAME);
		
		$statement->execute();
		
		$result = $statement->fetchAll();
		return $result;
	}

	
	public function insertName($name)
	{
		
		$statement = $this->db->prepare($this->INSERT_NAME);
		$statement->bindValue(1, $name);
		
		$statement->execute();
	}
	public function selectByName($id_name)
	{
		
		$statement = $this->db->prepare($this->SELECTBYNAME);
		$statement->bindValue(1,$id_name);
		$statement->execute();
		
		$result = $statement->fetchAll(PDO::FETCH_BOTH);
		return $result;
	}
	
	
}
?>
