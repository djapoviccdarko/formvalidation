<?php
$errors = isset($errors) ? $errors : [];

$car = isset($car) ? $car : "";
$price = isset($price) ? $price : "";
$year = isset($year) ? $year : "";
$date = isset($date) ? $date : "";
require_once 'DAONames.php';

$daoNames = new DAONames();
$names = $daoNames->selectNames();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <link rel="stylesheet" href="car-form.css">
    <title>Document</title>
</head>

<body>
    <?php include_once("./partials/nav.php") ?>
    <div class="container">
        <div class="col-md-12 add-car">
            <h1>Add car</h1>
            <form action="controller2.php" method="POST" class="form-content" name="addcar">
                <span> Car brand:<br>
                    <select name="car">
                        <?php foreach ($names as $pom) { ?>
                            <option value="<?= $pom['id_name'] ?>"><?= $pom['name'] ?></option>
                        <?php } ?>
                    </select><br>
                    <span><b> Enter car price :</b></span> <br> <input type="text" name="price" value="<?= $price ?>"><span style="color: red;"><?= isset($errors['price']) ? $errors['price'] : '' ?></span><br>
                    <span><b> Year of manufacture :</b></span> <br> <input type="text" name="year" value="<?= $year ?>"><span style="color: red;"><?= isset($errors['year']) ? $errors['year'] : '' ?></span><br>
                    <span><b> Date of purchase :</b></span> <br> <input type="text" name="date" value="<?= $date ?>"><br><span style="color: red;"><?= isset($errors['date']) ? $errors['date'] : '' ?></span><br>
                    <span style="color: red;"><?= isset($errors['form']) ? $errors['form'] : '' ?></span>
                    <input type="submit" value="Send" name="action"><br>
                    <input type="submit" value="View all" name="action">
            </form>
            <form action="controller2.php" method="GET">
                <span><b> Year of manufacture :</b></span> <br> <input type="text" name="year" value="<?= $year ?>"><span style="color: red;"><?= isset($errors['year']) ? $errors['year'] : '' ?></span><br>
                <input type="submit" value="View cars" name="action">
            </form>
            <form action="controller2.php" method="GET">
                <span><b> Choose the day of purchase :</b></span> <br> <input type="text" name="date" value="<?= $date ?>"><span style="color: red;"><?= isset($errors['date']) ? $errors['date'] : '' ?></span><br>
                <input type="submit" value="Choose day" name="action">
            </form>
            <h2>Prikaz po zaduzenju</h2>
            <form action="controller2.php" method="GET" class="form-content" name="addcar">
                <span> Car:<br>
                    <select name="id_name">
                        <?php foreach ($names as $pom) { ?>
                            <option value="<?= $pom['id_name'] ?>"><?= $pom['name'] ?></option>
                        <?php } ?>
                    </select><br>
                    <input type="submit" name="action" value="Prikaz zaduzenja">
                        </form>
        </div>
    </div>
</body>

</html>