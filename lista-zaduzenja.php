<?php 
require_once 'car-controller.php';
require_once 'DAO.php';
$zaduzenja = isset($zaduzenja)? $zaduzenja:[];

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <link rel="stylesheet" href="car-form.css">
    <title>Document</title>
</head>
<body>
    <?php include_once ("./partials/nav.php") ?>
    <div class="container">
        <div class="col-md-12" style="margin-top: 5rem;">
<table class="table" style="color: white;"> 
<p style="color: white;"> Table of all cars:</p>
    <tr>
    <th>RB</th>
            <th>Marka</th>
            <th>Cena</th>
            <th>Godiste automobila</th>
            <th>Dan Kupovine</th>
            <th>Ime</th>
            <th>Prezime</th>
            <th>Vreme zaduzenja</th>
            <th>Vreme razduzenja</th>
            <th>Izmeni - vezba</th>
            <th>Obrisi - vezba</th>
    </tr>
<?php 

foreach ($zaduzenja as $pom){ ?>
    <tr>
    <td><?= $pom['id_car'] ?></td>
            <td><?= $pom['name'] ?></td>
            <td><?= $pom['price'] ?></td>
            <td><?= $pom[14] ?></td>
            <td><?= $pom['purchase'] ?></td>
            <td><?= $pom['first_name'] ?></td>
            <td><?= $pom['last_name'] ?></td>
            <td><?= $pom['indebtedness_time'] ?></td>
            <td><?= $pom['discharge_time'] ?></td>
            <td><a href="#">Izmeni</a> </td>
            <td><a href="#">Obrisi</a> </td>
    </tr>
    <?php }
    ?>
</table>
</div>
    </div>
</body>
</html>