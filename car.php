<?php 
require_once 'car-controller.php';
require_once 'DAO.php';
$dao = new DAO();
$cars=$dao->selectCars();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <link rel="stylesheet" href="car-form.css">
    <title>Document</title>
</head>
<body>
<?php include_once ("./partials/nav.php") ?>
<?php if(count($errors)==0 && isset($_POST['action']) && $_POST['action'] == "Send"){ ?>
<div class="container">
    <div class="col-md-12 add-car">
    <table class="table" style="color: white;">
    <tr>
    <th>Number</th>
    <th>Car brand</th>
    <th>Price</th>
    <th>Year of manufacture</th>
    <th>Date of purchase</th>
    </tr>
    <?php $br=0; ?>
    <tr>
        <td><?=$br+1  ?></td>
        <td><?=$car  ?></td>
        <td><?=$price  ?></td>
        <td><?=$year  ?></td>
        <td><?=$date  ?></td>
    </tr>
    </table>
    </div>
    <?php } ?>
  
    
<?php if (isset($_GET['action']) && $_GET['action'] == "Choose day") { ?>
    <div class="container">
    <div class="col-md-12 add-car">
<table class="table" style="color: white;">
Per date:
    <tr>
        <th>Number</th>
        <th>Name</th>
        <th>Price</th>
        <th>Year</th>
        <th>Date</th>
    </tr>
<?php 
$br=1;
foreach ($cars as $carr){
    if($carr['purchase'] == $date){
     ?>
    <tr>
    <td><?=$carr['id_car'] ?></td>
        <td><?=$carr['name'] ?></td>
        <td><?=$carr['price'] ?></td>
        <td><?=$carr['year'] ?></td>
        <td><?=$carr['purchase'] ?></td>
</tr>
</div>
    <?php }  
}

}
    ?>
</div>
</table>
</div>
</body>
</html>