<?php 
$errors = isset($errors)?$errors:[];
$cars = isset($cars)?$cars:'';
require_once 'DAONames.php';

$daoNames = new DAONames();
$names = $daoNames->selectNames();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <link rel="stylesheet" href="car-form.css">
    <title>Document</title>
</head>
<body>
<?php include_once ("./partials/nav.php") ?>
<div class="container">
    <div class="col-md-12 add-car">
    <h1>Add car</h1>
    <form action="controller2.php" method="POST" class="form-content">
    Car brand:<br>
    <select name="id_name">
        <?php foreach($names as $pom){ ?>
        <option value="<?= $pom['id_name'] ?>"  <?=  $cars['id_name']==$pom['id_name']? 'selected':'' ?> ><?= $pom['name'] ?></option>
        <?php }?>
    </select><br>
    Enter car price:<br>
    <input type="text" name="price" value="<?= $cars['price']?>"><br>
    Year of manufacture:<br>
    <input type="text" name="year" value="<?= $cars['year']?>"><br>
    Date of purchase :<br>
    <input type="text" name="purchase" value="<?= $cars['purchase']?>"><br>
    <input type="hidden" name="id_car" value="<?= $cars['id_car']?>">
    <input type="submit" name="action" value="Edit">
    
</form>

</div>
</body>
</html>