<?php 
$errors = isset($errors)?$errors:[];

$name = isset($name)?$name: "";
$lastname = isset($lastname)?$lastname: "";
$date = isset($date)?$date: "";
$email = isset($email)?$email: "";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <link rel="stylesheet" href="register.css">
    <title>LogIn</title>
</head>

<body>
    <?php include_once("./partials/nav.php") ?>
    <div class="container">
        <div class="col-md-12 register">
            <h1>Register</h1>
            <form action="controller.php" method="GET" class="form-content" name="registration">
                <span><b> Enter your first name:</b></span> <br> <input type="text" name="name" value="<?= $name ?>"><br>
                <span><b> Enter your last name: </b></span><br> <input type="text" name="lastname" value="<?= $lastname ?>"><br>
                <span><b>Birth year: </b></span><br> <input type="text" name="date" value="<?= $date ?>"><span style="color: red;"><?=isset($errors['date'])?$errors['date']:'' ?></span><br>
                <span><b>Enter your e-mail address: </b></span><br> <input type="text" name="email" value="<?= $email ?>"><br>
                <span style="color: red;"><?=isset($errors['form'])?$errors['form']:'' ?></span>
                <input type="submit" value="Send" name="action"><br>
                
                <?php
                /*
                if(isset($_GET['action']) && $_GET['action'] == "Send"){
                    if (count($errors) == 0) {
                        echo "<span style='color:red'>";
                        echo 'Uspesan unos!<br>Uneli ste:<br>';
                        echo $name . ' ' . $lastname . ' <br> ' . $date . ' <br> ' . $email;
                       echo "</span>";
                    } else {
                        //var_dump($errors);
                        echo '<ul>';
                        foreach ($errors as $err)
                            echo "<li style='color: blue;'>$err</li>";
                        echo '</ul>';
                    }
                    
                    }
                    */
                    
                ?>
            </form>
        </div>
    </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

</html>