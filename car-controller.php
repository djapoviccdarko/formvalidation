<?php 
  require_once 'DAO.php';
  require_once 'DAONames.php';
class CarController{
    public function sendButton(){
        $errors = [];
        $car = isset($_POST['car']) ? $_POST['car'] : "";
        $price = isset($_POST['price']) ? $_POST['price'] : "";
        $year = isset($_POST['year']) ? $_POST['year'] : "";
        $date = isset($_POST['date']) ? $_POST['date'] : "";
        if ($car == "" || $year == '' || $price == "" || $date == "") {
            $errors['form'] = '<b>Fill in all fields!</b><br>';
            include_once 'car-form.php';
        }
        if ($price < 0) {
            $errors['price'] = '<b>Price is not valid!</b><br>';
            include_once 'car-form.php';
        }
        if ($year > 2022) {
            $errors['year'] = '<b>Year is not valid!</b><br>';
            include_once 'car-form.php';
        }
        if (!(date("Y-m-d", strtotime($date)) == $date)) {
            $errors['date'] = '<b>Date is not valid</b><br>';
            include_once 'car-form.php';
        } elseif (date("Y-m-d", strtotime($date)) > date("Y-m-d")) {
            $errors['date'] =  '<b>The date must be in the past!</b><br>';
            include_once 'car-form.php';
        }
        if (count($errors) == 0 && isset($_POST['action']) && $_POST['action'] == "Send") {
            include_once 'car.php';
            $dao = new DAO();
            $dao->insertCar($car,$price,$year,$date);
            }
        }
       

        public function viewCarsButton(){
            $errors = [];
            $car = isset($_GET['car']) ? $_GET['car'] : "";
            $price = isset($_GET['price']) ? $_GET['price'] : "";
            $year = isset($_GET['year']) ? $_GET['year'] : "";
            $date = isset($_GET['date']) ? $_GET['date'] : "";
            if (isset($_GET['action']) && $_GET['action'] == "View cars") {
                $dao= new DAO();
                $cars = $dao->selectCars();
                $dao->getCarByYear($year);
                include_once 'car-list.php';
            }
            
        }
        public function chooseDayButton(){
             $errors = [];
            $car = isset($_GET['car']) ? $_GET['car'] : "";
            $price = isset($_GET['price']) ? $_GET['price'] : "";
            $year = isset($_GET['year']) ? $_GET['year'] : "";
            $date = isset($_GET['date']) ? $_GET['date'] : "";
            
            if (isset($_GET['action']) && $_GET['action'] == "Choose day") {
                include_once 'car.php';
                $dao = new DAO();
            $dao->getCarByDate($date);
            }
        }
        public function viewAllButton(){
            $errors = [];
            $car = isset($_POST['car']) ? $_POST['car'] : "";
            $price = isset($_POST['price']) ? $_POST['price'] : "";
            $year = isset($_POST['year']) ? $_POST['year'] : "";
            $date = isset($_POST['date']) ? $_POST['date'] : "";
            if (count($errors) == 0 && isset($_POST['action']) && $_POST['action'] == "View all") {
                $dao = new DAO();
                $cars = $dao->selectCars();
                include_once 'car-list.php';
                
            }
        }
        public function delete(){
            $dao=new DAO();
            $id=isset($_GET['id'])?$_GET['id'] : '';
            $dao->deleteCar($_GET['id']);
            $cars = $dao->selectCars();
            include_once 'car-list.php';
        }

        public function edit(){
            $dao=new DAO();
            $id=isset($_GET['id'])?$_GET['id'] : '';
            $cars=$dao->getCarById($id);
            include_once 'edit-car.php';
        }
        public function saveEdit(){
            $dao=new DAO();
            $dao->updateCar($_POST['id_name'], $_POST['price'], $_POST['year'], $_POST['purchase'], $_POST['id_car']);
            $cars = $dao->selectCars();
             include_once 'car-list.php';

        }
        public function prikazZaduzenja(){
            $id_name = isset($_GET['id_name']) ? $_GET['id_name'] : '';
            if ($id_name == '') {
                include_once 'car-form.php';
            } else {
                $dao = new DAONames();
                $zaduzenja = $dao->selectByName($id_name);
                //var_dump($zaduzenja);
                //die();
                include_once 'lista-zaduzenja.php';
            }
        }

    }
