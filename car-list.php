<?php 
require_once 'car-controller.php';
require_once 'DAO.php';

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <link rel="stylesheet" href="car-form.css">
    <title>Document</title>
</head>
<body>
    <?php include_once ("./partials/nav.php") ?>
    <div class="container">
        <div class="col-md-12" style="margin-top: 5rem;">
<table class="table" style="color: white;"> 
<p style="color: white;"> Table of all cars:</p>
    <tr>
        <th>Number</th>
        <th>Name</th>
        <th>Price</th>
        <th>Year</th>
        <th>Date</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
<?php 

foreach ($cars as $carr){ ?>
    <tr>
        <td><?=$carr['id_car']  ?></td>
        <td><?=$carr['name'] ?></td>
        <td><?=$carr['price'] ?></td>
        <td><?=$carr['year'] ?></td>
        <td><?=$carr['purchase'] ?></td>
        <td><a href="controller2.php?action=edit&id=<?=$carr["id_car"] ?>">Edit</a></td>
        <td><a href="controller2.php?action=delete&id=<?=$carr["id_car"] ?>">Delete</a></td>
        
    </tr>
    <?php }
    ?>
</table>
<?php if (isset($_GET['action']) && $_GET['action'] == "View cars") { ?>
<table class="table" style="color: white;">
Year:
    <tr>
        <th>Number</th>
        <th>Name</th>
        <th>Price</th>
        <th>Year</th>
        <th>Date</th>
    </tr>
<?php 
$br=1;
foreach ($cars as $carr){
    if($carr['year'] == $year){
     ?>
    <tr>
        <td><?=$carr['id_car'] ?></td>
        <td><?=$carr['name'] ?></td>
        <td><?=$carr['price'] ?></td>
        <td><?=$carr['year'] ?></td>
        <td><?=$carr['purchase'] ?></td>
</tr>
    <?php }  
}
}
    ?>

</table>
</div>
    </div>
</body>
</html>