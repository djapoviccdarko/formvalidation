<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <link rel="stylesheet" href="person.css">
    <title>Person</title>
</head>
<body>
<?php include_once("./partials/nav.php") ?>
<table class="table">
    <tr>
    <th>Number</th>
    <th>Name</th>
    <th>Last Name</th>
    <th>Birth year</th>
    <th>E-mail</th>
    </tr>
    <?php $br=0; ?>
    <tr>
        <td><?=$br+1  ?></td>
        <td><?=$name  ?></td>
        <td><?=$lastname  ?></td>
        <td><?=$date  ?></td>
        <td><?=$email  ?></td>
    </tr>
</table>
</body>
</html>