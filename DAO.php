<?php
require_once 'db.php';

class DAO {
	private $db;
	private $INSERTCAR = "INSERT INTO cars (id_name, price, year,purchase) VALUES (?, ?,?,?)";
	private $UPDATECAR = "UPDATE cars SET id_name= ?, price = ?, year = ?, purchase=? WHERE id_car = ?";
	private $DELETECAR = "DELETE  FROM cars WHERE id_car = ?";
	private $SELECTBYID = "SELECT * FROM cars WHERE id_car = ?";	
	private $SELECTBYDATE = "SELECT * FROM cars WHERE purchase = ?";	
	private $SELECTBYYEAR = "SELECT * FROM cars WHERE year = ?";	
	private $GETLASTCAR = "SELECT * FROM cars ORDER BY id_car ASC LIMIT ?";
	private $GETCARBYNAME = "SELECT * FROM cars WHERE id_name= ?";
	private $SELECTCARS = "SELECT * FROM cars JOIN names on cars.id_name = names.id_name";

	
	public function __construct()
	{
		$this->db = DB::createInstance();
	}
	public function selectCars()
	{
		
		$statement = $this->db->prepare($this->SELECTCARS);
		
		$statement->execute();
		
		$result = $statement->fetchAll();
		return $result;
	}
	public function getLastNCar($n)
	{
		$statement = $this->db->prepare($this->GETLASTCAR);
		$statement->bindValue(1, $n, PDO::PARAM_INT);
		$statement->execute();
		
		$result = $statement->fetchAll();
		return $result;
	}

	public function insertCar($name, $price, $year,$purchase)
	{

		$statement = $this->db->prepare($this->INSERTCAR);
		$statement->bindValue(1, $name);
		$statement->bindValue(2, $price);
		$statement->bindValue(3, $year);
		$statement->bindValue(4, $purchase);
		$statement->execute();
	}
	public function updateCar($name, $price, $year, $purchase, $id)
	{
		
		$statement = $this->db->prepare($this->UPDATECAR);
		$statement->bindValue(1, $name);
		$statement->bindValue(2, $price);
		$statement->bindValue(3, $year);
		$statement->bindValue(4, $purchase);
		$statement->bindValue(5, $id);
		
		$statement->execute();
	}
	public function deleteCar($id)
	{
		$statement = $this->db->prepare($this->DELETECAR);
		$statement->bindValue(1, $id);
		
		$statement->execute();
	}
	public function getCarByName($name)
	{
		$statement = $this->db->prepare($this->GETCARBYNAME);
		$statement->bindValue(1, $name);
		
		$statement->execute();
		
		$result = $statement->fetchAll();
		return $result;
	}
	public function getCarById($id)
	{
		$statement = $this->db->prepare($this->SELECTBYID);
		$statement->bindValue(1, $id);
		
		$statement->execute();
		
		$result = $statement->fetch();
		return $result;
	}
	public function getCarByDate($date)
	{
		$statement = $this->db->prepare($this->SELECTBYDATE);
		$statement->bindValue(1, $date);
		
		$statement->execute();
		
		$result = $statement->fetch();
		return $result;
	}
	public function getCarByYear($year)
	{
		$statement = $this->db->prepare($this->SELECTBYYEAR);
		$statement->bindValue(1, $year);
		
		$statement->execute();
		
		$result = $statement->fetch();
		return $result;
	}
}
?>